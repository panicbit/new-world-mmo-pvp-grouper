use std::collections::HashSet;

use prettytable::{format, Cell, Table};

use crate::data_types::{GroupType, Raid, Role};
use crate::PlayersType;

#[allow(dead_code)]
pub(crate) fn print_placed_players_long_version(raid: &Raid, players: &PlayersType) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    // table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

    let mut players_print_result = players.clone();

    let role_display_order = [
        Role::Initiation,
        Role::Initiation,
        Role::Tank,
        Role::Melee,
        Role::Aoe,
        Role::Aoe,
        Role::Range,
        Role::Range,
        Role::Healer,
        Role::Shotcaller,
    ];
    table.set_titles(row![
        "Type",
        "Initiation",
        "Initiation",
        "Tank",
        "Melee",
        "AOE",
        "AOE",
        "Range",
        "Range",
        "Healer",
        "Shotcaller",
        "Sum",
    ]);
    let mut group_role_used;
    let mut player_in_group;
    for g in &raid.groups {
        if g.group_type == GroupType::Siege || g.group_type == GroupType::SiegeCaller {
            continue;
        }
        player_in_group = 0;
        let mut row = prettytable::Row::new(vec![]);
        row.add_cell(Cell::new(&*g.group_type.to_string()));
        for role in &role_display_order {
            group_role_used = false;
            for p in &g.players {
                if !group_role_used
                    && players_print_result.contains_key(p)
                    && &players_print_result[p].calculated_role == role
                {
                    row.add_cell(Cell::new(p));
                    group_role_used = true;
                    player_in_group += 1;
                    players_print_result.remove(p);
                }
            }
            if !group_role_used {
                row.add_cell(Cell::new(""));
            }
        }
        row.add_cell(Cell::new(&*player_in_group.to_string()));
        table.add_row(row);
    }
    table.printstd();
}

pub(crate) fn print_placed_players_short_version(raid: &Raid, players: &PlayersType) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    // table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

    table.set_titles(row![
        "Group Type",
        "Player 1",
        "Player 2",
        "Player 3",
        "Player 4",
        "Player 5",
    ]);

    let roles_sort_order = [
        Role::Initiation,
        Role::Tank,
        Role::Melee,
        Role::Aoe,
        Role::Range,
        Role::Healer,
        Role::Shotcaller,
    ];

    for g in &raid.groups {
        let mut row = prettytable::Row::new(vec![]);
        row.add_cell(Cell::new(&*g.group_type.to_string()));
        for next_role in &roles_sort_order {
            for p in &g.players {
                if players[p].calculated_role != *next_role {
                    continue;
                }
                row.add_cell(Cell::new(&*format!(
                    "({}) {}",
                    players[p]
                        .calculated_role
                        .to_string()
                        .chars()
                        .next()
                        .unwrap(),
                    p,
                )));
            }
        }
        for _i in g.players.len()..5 {
            row.add_cell(Cell::new(""));
        }
        table.add_row(row);
    }
    table.printstd();
}

pub(crate) fn print_unplaced_players(players: &PlayersType, player_avial: &HashSet<String>) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);

    // let mut unplaced_players = players.clone();
    // for g in &raid.groups {
    //     for p in &g.players {
    //         unplaced_players.remove(&*p);
    //     }
    // }

    let roles = [
        Role::Initiation,
        Role::Tank,
        Role::Melee,
        Role::Aoe,
        Role::Range,
        Role::Healer,
        Role::Shotcaller,
    ];
    let mut player_by_role = vec![];
    for player_role in roles {
        let mut player_list: Vec<String> = vec![];
        for (name, _role) in players
            .iter()
            .filter(|&(_name, player)| player.calculated_role == player_role)
        {
            if player_avial.contains(name) {
                player_list.push(name.clone());
            }
        }
        player_by_role.push(player_list);
    }

    table.set_titles(row![
        "Initiation",
        "Tank",
        "Melee",
        "AOE",
        "Range",
        "Healer",
        "Shotcaller"
    ]);
    let mut printed = true;
    while printed {
        printed = false;
        let mut row = prettytable::Row::new(vec![]);
        for player_with_role in &mut player_by_role {
            if !player_with_role.is_empty() {
                printed = true;
                row.add_cell(Cell::new(&player_with_role.pop().unwrap()));
            } else {
                row.add_cell(Cell::new(""));
            }
        }
        table.add_row(row);
    }
    table.printstd();
}
