use std::collections::HashMap;
use std::fs::File;
use std::path::PathBuf;

use crate::data_types::PredefinedGroup;
use crate::data_types::Role;

#[allow(dead_code)]
fn read_roles_csv() -> HashMap<String, Role> {
    let mut roles_filename = dirs::download_dir().expect("cannot detect path to download dir!");
    roles_filename.push("New-World - Roles.csv");

    let mut players: HashMap<String, Role> = HashMap::new();

    let file = File::open(&roles_filename).unwrap_or_else(|_| {
        panic!(
            "File {} not found",
            roles_filename.as_path().to_str().unwrap()
        )
    });
    let mut reader = csv::ReaderBuilder::new()
        // .delimiter(delimiter)
        .has_headers(true)
        .flexible(false)
        .from_reader(file);
    while let Some(result) = reader.records().next() {
        // for record in reader.records() {
        let record = result.unwrap();
        let player_name = String::from(record[0].to_string().trim());
        if player_name.is_empty() {
            continue;
        }
        let player_role = record[1].to_string().trim().to_lowercase();
        let player_role = match player_role.as_ref() {
            "melee" => Role::Melee,
            "range" => Role::Range,
            "healer" => Role::Healer,
            "tank" => Role::Tank,
            _ => panic!("Player {} has illegal role {}!", player_name, player_role),
        };
        let name = player_name.clone().parse().unwrap();
        let role = player_role;

        players.insert(name, role);
    }
    players
}

#[allow(dead_code)]
fn read_setup_csv(setup_filename: PathBuf) -> Vec<PredefinedGroup> {
    let mut setups: Vec<PredefinedGroup> = Vec::new();

    let file = File::open(&setup_filename).unwrap_or_else(|_| {
        panic!(
            "File {} not found",
            setup_filename.as_path().to_str().unwrap()
        )
    });
    let mut reader = csv::ReaderBuilder::new()
        // .delimiter(delimiter)
        .has_headers(true)
        .flexible(false)
        .from_reader(file);
    while let Some(result) = reader.records().next() {
        // for record in reader.records() {
        let record = result.unwrap();
        let player_name = String::from(record[0].to_string().trim());
        let player_group_type = record[1].to_string().trim().to_lowercase();
        let player_group_type = crate::util::string_to_group_type(player_group_type);
        let mut setup = PredefinedGroup {
            group_leader: player_name,
            group_type: player_group_type,
            group_members: vec![],
        };
        for i in 2..record.len() {
            let name: String = String::from(record[i].to_string().trim());
            if !name.is_empty() {
                setup.group_members.push(name)
            }
        }
        setups.push(setup);
    }
    setups
}

#[allow(dead_code)]
fn read_player_skill_csv(roles_filename: PathBuf) -> HashMap<String, usize> {
    let mut players: HashMap<String, usize> = HashMap::new();

    let file = File::open(&roles_filename).unwrap_or_else(|_| {
        panic!(
            "File {} not found",
            roles_filename.as_path().to_str().unwrap()
        )
    });
    let mut reader = csv::ReaderBuilder::new()
        // .delimiter(delimiter)
        .has_headers(true)
        .flexible(false)
        .from_reader(file);
    while let Some(result) = reader.records().next() {
        let record = result.unwrap();
        let player_name = String::from(record[0].to_string().trim());
        let player_skill = record[1].parse::<usize>().expect(&*format!(
            "Skill for player {} is not a number.",
            &player_name
        ));
        players.insert(player_name, player_skill);
    }
    players
}
