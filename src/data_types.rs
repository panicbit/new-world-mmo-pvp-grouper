use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, strum_macros::ToString, PartialEq, Eq, Hash, Clone)]
pub enum Role {
    Healer,
    Melee,
    Range,
    Tank,
    Initiation,
    Aoe,
    Shotcaller,
}

#[derive(Debug, strum_macros::ToString, PartialEq, Eq, Hash)]
pub enum GroupType {
    MainLeft,
    MainRight,
    FlexLeft,
    FlexRight,
    Siege,
    SiegeCaller,
}

#[derive(Debug, Clone)]
pub enum Weapon {
    Sword,
    Hatchet,
    Rapier,
    Greataxe,
    Warhammer,
    Spear,
    Fire,
    Ice,
    Life,
    Bow,
    Musket,
}

#[derive(Debug, Clone)]
pub(crate) struct Player {
    pub(crate) id: usize,
    pub(crate) name: String,
    pub(crate) signup_role: Role, // player self assigned role (Healer, Melee, Range, Tank)
    pub(crate) calculated_role: Role, // this role can also be Initiation, Aoe and Shotcaller, determined by this program
    pub(crate) level: usize,
    pub(crate) weapon_1: Weapon,
    pub(crate) weapon_2: Weapon,
    pub(crate) gear_score: usize,
    pub(crate) competence: usize, // how well the player handles his char
    pub(crate) from_setup: bool,  // is the player part of a predefined group
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct PlayerJson {
    pub(crate) sign_up_1: String,
    pub(crate) sign_up_2: String,
    pub(crate) sign_up_3: String,
    pub(crate) name: String,
    pub(crate) role: String,
    pub(crate) level: String,
    pub(crate) weapon_1: String,
    pub(crate) weapon_2: String,
    pub(crate) gear_score: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub(crate) struct RolesJson {
    pub(crate) range: String,
    pub(crate) majorDimension: String,
    pub(crate) values: Vec<PlayerJson>,
}

#[derive(Debug)]
pub(crate) struct PredefinedGroup {
    pub(crate) group_leader: String,
    pub(crate) group_type: GroupType,
    pub(crate) group_members: Vec<String>,
}

#[derive(Debug)]
pub(crate) struct Group {
    pub(crate) role_to_count: HashMap<Role, usize>,
    pub(crate) group_type: GroupType,
    pub(crate) players: Vec<String>,
}

#[derive(Debug)]
pub(crate) struct Raid {
    pub(crate) groups: Vec<Group>,
}
