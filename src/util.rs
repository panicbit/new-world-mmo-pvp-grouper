use std::collections::{HashMap, HashSet};
use std::{env, process};

use pickledb::{PickleDb, PickleDbDumpPolicy, SerializationMethod};

use crate::data_types::{Group, GroupType, Player, PlayerJson, Raid, Role, Weapon};
use crate::{read_from_google_sheets, PlayersType};

pub(crate) fn sort_players_for_raid(players: &PlayersType) -> Vec<String> {
    #[derive(Debug)]
    struct PlayerSorting {
        name: String,
        level: usize,
        gear_score: usize,
        competence: usize,
    }

    let mut players_for_sorting: Vec<PlayerSorting> = Vec::new();
    // Todo this whole thing should be doable with map()
    for p in players.values() {
        let ps = PlayerSorting {
            name: p.name.clone(),
            level: p.level,
            gear_score: p.gear_score,
            competence: p.competence,
        };
        players_for_sorting.push(ps);
    }
    // Remember: this sorting has to be done in reverse order of importance.
    players_for_sorting.sort_by_key(|p| p.gear_score);
    players_for_sorting.sort_by_key(|p| p.level);
    players_for_sorting.sort_by_key(|p| p.competence);
    players_for_sorting.reverse();

    let show_sort = match env::args().nth(2) {
        None => false,
        Some(tab) => tab == "showsort",
    };

    if show_sort {
        for p in &players_for_sorting {
            println!("sorted: {:?}", p)
        }
    }

    players_for_sorting.iter().map(|p| p.name.clone()).collect()
}

pub(crate) fn create_empty_raid() -> Raid {
    let mut raid = Raid { groups: vec![] };
    let group_setup: [GroupType; 10] = [
        GroupType::MainLeft,
        GroupType::MainLeft,
        GroupType::MainLeft,
        GroupType::MainRight,
        GroupType::MainRight,
        GroupType::MainRight,
        GroupType::FlexLeft,
        GroupType::FlexRight,
        GroupType::Siege,
        GroupType::SiegeCaller,
    ];
    for group_type in group_setup {
        let mut role_to_count: HashMap<Role, usize> = HashMap::new();
        role_to_count.insert(Role::Healer, 0);
        role_to_count.insert(Role::Melee, 0);
        role_to_count.insert(Role::Range, 0);
        role_to_count.insert(Role::Tank, 0);
        role_to_count.insert(Role::Shotcaller, 0);
        role_to_count.insert(Role::Aoe, 0);
        role_to_count.insert(Role::Initiation, 0);
        let group = Group {
            role_to_count: role_to_count,
            group_type,
            players: vec![],
        };
        raid.groups.push(group);
    }
    raid
}

fn string_to_role(role: &str) -> Role {
    match role.to_lowercase().as_ref() {
        "melee" => Role::Melee,
        "range" => Role::Range,
        "healer" => Role::Healer,
        "tank" => Role::Tank,
        _ => {
            panic!("illegal role found: {}!", role);
        }
    }
}

fn string_to_weapon(weapon: &str) -> Weapon {
    match weapon.trim().to_lowercase().as_ref() {
        "sword" => Weapon::Sword,
        "hatchet" => Weapon::Hatchet,
        "rapier" => Weapon::Rapier,
        "greataxe" => Weapon::Greataxe,
        "warhammer" => Weapon::Warhammer,
        "spear" => Weapon::Spear,
        "fire" => Weapon::Fire,
        "ice" => Weapon::Ice,
        "life" => Weapon::Life,
        "bow" => Weapon::Bow,
        "musket" => Weapon::Musket,
        _ => {
            panic!("illegal weapon found: {}", weapon);
        }
    }
}

pub(crate) fn is_spot_open_for_role(group: &Group, role: &Role) -> bool {
    // wee need this check since predefined groups don't follow role restrictions
    if group.role_to_count.values().sum::<usize>() == 5 {
        return false;
    }
    match group.group_type {
        GroupType::MainLeft | GroupType::MainRight => match role {
            Role::Initiation => group.role_to_count[&Role::Initiation] == 0,
            Role::Tank | Role::Melee => {
                group.role_to_count[&Role::Tank] + group.role_to_count[&Role::Melee] < 1
            }
            Role::Healer => group.role_to_count[&Role::Healer] == 0,
            Role::Aoe | Role::Range => {
                group.role_to_count[&Role::Aoe] + group.role_to_count[&Role::Range] < 2
            }
            Role::Shotcaller => false,
        },
        GroupType::FlexLeft | GroupType::FlexRight => match role {
            Role::Initiation => group.role_to_count[&Role::Initiation] < 2,
            Role::Tank | Role::Melee => false,
            Role::Healer => group.role_to_count[&Role::Healer] == 0,
            Role::Aoe | Role::Range => {
                group.role_to_count[&Role::Aoe] + group.role_to_count[&Role::Range] < 2
            }
            Role::Shotcaller => false,
        },
        GroupType::Siege => match role {
            Role::Initiation => false,
            Role::Tank | Role::Healer | Role::Aoe => {
                group.role_to_count[&Role::Tank]
                    + group.role_to_count[&Role::Healer]
                    + group.role_to_count[&Role::Aoe]
                    < 5
            }
            Role::Melee => false,
            Role::Range => false,
            Role::Shotcaller => false,
        },
        GroupType::SiegeCaller => match role {
            Role::Initiation => false,
            Role::Tank | Role::Healer | Role::Aoe => {
                group.role_to_count[&Role::Tank]
                    + group.role_to_count[&Role::Healer]
                    + group.role_to_count[&Role::Aoe]
                    < 4
            }
            Role::Melee => false,
            Role::Range => false,
            Role::Shotcaller => group.role_to_count[&Role::Shotcaller] == 0,
        },
    }
}

pub(crate) fn build_players_data_structure() -> PlayersType {
    let players_json = read_from_google_sheets::read_signed_up_players_from_google_sheet();
    let mut players: PlayersType = HashMap::new();

    let player_competence = read_from_google_sheets::read_player_competence_rest();
    for (id, player) in players_json.iter().enumerate() {
        let new_player = Player {
            id,
            name: player.name.clone(),
            signup_role: string_to_role(&player.role),
            calculated_role: player_to_role(player),
            level: player
                .level
                .parse::<usize>()
                .expect(&*format!("level {} is not a number!", player.level)),
            weapon_1: string_to_weapon(&player.weapon_1),
            weapon_2: string_to_weapon(&player.weapon_2),
            gear_score: player.gear_score.parse::<usize>().expect(&*format!(
                "gear score {} is not a number!",
                player.gear_score
            )),
            competence: *player_competence.get(&player.name).unwrap_or(&2),
            from_setup: false, // default
        };
        if players.contains_key(&*player.name) {
            println!("player {} found more than once!", player.name);
            process::exit(1);
        }
        players.insert(player.name.clone(), new_player);
    }
    players
}

pub(crate) fn build_player_avail_data_structure(players: &PlayersType) -> HashSet<String> {
    let mut player_avail: HashSet<String> = HashSet::new();
    for player in players.keys() {
        player_avail.insert(player.clone());
    }
    player_avail
}

pub(crate) fn string_to_group_type(group_type: String) -> GroupType {
    match group_type.as_ref() {
        "mainleft" => GroupType::MainLeft,
        "mainright" => GroupType::MainRight,
        "flexleft" => GroupType::FlexLeft,
        "flexright" => GroupType::FlexRight,
        "siege" => GroupType::Siege,
        _ => panic!("illegal group type found: {}", group_type),
    }
}

fn player_to_role(player: &PlayerJson) -> Role {
    if player.name == "dwn" || player.name == "Enlit" {
        return Role::Shotcaller;
    };

    if player.role != "Healer" && (player.weapon_1 == "Greataxe" || player.weapon_2 == "Greataxe") {
        Role::Initiation
    } else if (player.weapon_1 == "Sword" || player.weapon_1 == "Warhammer")
        && (player.weapon_1 != "Greataxe" && player.weapon_2 != "Greataxe")
        || (player.weapon_2 == "Sword" || player.weapon_2 == "Warhammer")
            && (player.weapon_1 != "Greataxe" && player.weapon_2 != "Greataxe")
    {
        Role::Tank
    } else if (player.weapon_1 == "Life" || player.weapon_2 == "Life") && player.role == "Healer" {
        Role::Healer
    } else if (player.weapon_1 == "Fire"
        || player.weapon_1 == "Ice"
        || player.weapon_1 == "Bow"
        || player.weapon_2 == "Fire"
        || player.weapon_2 == "Ice"
        || player.weapon_2 == "Bow")
        && player.role != "Healer"
    {
        Role::Aoe
    } else if player.weapon_1 == "Musket" || player.weapon_2 == "Musket" {
        Role::Range
    } else if player.weapon_1 == "Spear"
        || player.weapon_1 == "Hatchet"
        || player.weapon_1 == "Rapier"
        || player.weapon_2 == "Spear"
        || player.weapon_2 == "Hatchet"
        || player.weapon_2 == "Rapier"
    {
        Role::Melee
    } else {
        panic!("Can't assign role to player {:?}", player);
    }
}

pub fn read_api_key() -> String {
    let db = PickleDb::load(
        "key.db",
        PickleDbDumpPolicy::NeverDump,
        SerializationMethod::Json,
    )
    .expect("Could not open key.db {}");
    db.get::<String>("apikey").expect("Could not read api key.")
}
