use std::collections::{HashMap, HashSet};
use std::string::ToString;

use data_types::{Group, GroupType, Player, PredefinedGroup, Raid, Role};

#[macro_use]
extern crate prettytable;

mod data_types;
mod read_csv_files;
mod read_from_google_sheets;
mod util;
mod write_result_to_file;
mod write_results_to_stdout;

// build for Windows
// rustup target add x86_64-pc-windows-gnu
// rustup toolchain install stable-x86_64-pc-windows-gnu
// cargo build --release --target x86_64-pc-windows-gnu
// result .exe in /Users/jb/Documents/Projects/new_world_pvp_grouper/target/x86_64-pc-windows-gnu/release/deps

type PlayersType = HashMap<String, Player>;

fn main() {
    println!("New World PvP Group Builder by Jörn Bodemann, jb@opv.de");
    println!();

    let mut players = util::build_players_data_structure();
    let mut player_avail = util::build_player_avail_data_structure(&players);
    let predefined_groups = read_from_google_sheets::read_predefined_groups();

    let raid = build_raid(&mut players, &mut player_avail, &predefined_groups);

    // write_results_to_stdout::print_placed_players_long_version(&raid, &players);
    write_results_to_stdout::print_placed_players_short_version(&raid, &players);
    write_results_to_stdout::print_unplaced_players(&players, &player_avail);

    // write_unplaced_players_to_csv_file(&raid, &players);
    write_result_to_file::write_result_to_csv_file(&raid, &players);
    // write_result_to_file::write_unplaced_players_to_csv_file(&raid, &players);
}

fn build_raid(
    players: &PlayersType,
    player_avail: &mut HashSet<String>,
    predefined_groups: &[PredefinedGroup],
) -> Raid {
    let mut raid = util::create_empty_raid();

    add_predefined_groups_to_raid(&players, player_avail, predefined_groups, &mut raid);
    add_players_to_raid(&players, player_avail, &mut raid);

    raid
}

fn add_players_to_raid(
    players: &&PlayersType,
    player_avail: &mut HashSet<String>,
    mut raid: &mut Raid,
) {
    for player_name in util::sort_players_for_raid(players) {
        if player_avail.contains(&*player_name)
            && fill_first_avail_spot_in_raid(&mut raid, &player_name, players)
        {
            player_avail.remove(&player_name);
        }
    }
}

fn add_predefined_groups_to_raid(
    players: &PlayersType,
    player_avail: &mut HashSet<String>,
    predefined_groups: &[PredefinedGroup],
    raid: &mut Raid,
) {
    for predefined_group in predefined_groups {
        // Todo can we write this more idiomatic
        if !players.contains_key(&predefined_group.group_leader) {
            continue;
        }
        if let Some(group_number) = add_player_with_group_type_to_raid(
            raid,
            &predefined_group.group_leader,
            &predefined_group.group_type,
            &players[&predefined_group.group_leader].calculated_role,
        ) {
            player_avail.remove(&predefined_group.group_leader);
            for group_with_player_name in &predefined_group.group_members {
                if !players.contains_key(group_with_player_name) {
                    continue;
                }
                if add_player_to_group_ignoring_role(
                    &mut raid.groups[group_number],
                    group_with_player_name,
                    &players[group_with_player_name].calculated_role,
                ) {
                    player_avail.remove(group_with_player_name);
                }
            }
        };
    }
}

// inserts a player into the first group with matching group_type and available role spot
fn add_player_with_group_type_to_raid(
    raid: &mut Raid,
    name: &str,
    group_type: &GroupType,
    role: &Role,
) -> Option<usize> {
    for (i, group) in raid.groups.iter_mut().enumerate() {
        if group.group_type == *group_type && util::is_spot_open_for_role(group, role) {
            group.players.push(name.to_string());
            // println!("player added with group:{}", &name);
            *group.role_to_count.get_mut(role).unwrap() += 1;
            return Some(i);
        }
    }
    None
}
fn fill_first_avail_spot_in_raid(raid: &mut Raid, name: &str, players: &PlayersType) -> bool {
    for group in raid.groups.iter_mut() {
        if util::is_spot_open_for_role(group, &players[name].calculated_role) {
            group.players.push(name.to_string());
            *group
                .role_to_count
                .get_mut(&players[name].calculated_role)
                .unwrap() += 1;
            return true;
        }
    }
    false
}

// insert player into a specific group ignoring role requirements
fn add_player_to_group_ignoring_role(group: &mut Group, name: &str, role: &Role) -> bool {
    group.players.push(name.to_string());
    *group.role_to_count.get_mut(role).unwrap() += 1;
    true
}
