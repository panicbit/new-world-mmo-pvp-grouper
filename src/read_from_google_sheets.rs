use std::collections::HashMap;
use std::{env, process};

use serde::{Deserialize, Serialize};

use crate::data_types::PredefinedGroup;
use crate::data_types::{PlayerJson, RolesJson};

// reads the sign up players from a google sheet.
pub(crate) fn read_signed_up_players_from_google_sheet() -> Vec<PlayerJson> {
    let mut players: Vec<PlayerJson> = vec![];
    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1UqFimYMKmg5ZaLAKXt7A3kO4Erpdq2UNUis-J5DGNzQ";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let tab = match env::args().nth(1) {
        None => 1,
        Some(tab) => {
            if tab != "1" && tab != "2" && tab != "3" {
                1
            } else {
                tab.parse::<usize>().unwrap()
            }
        }
    };
    let url = format!(
        "{}{}{}{}{}{}",
        sheets_url, sheet_id, "/values/", "Sieges", "!A3:I200", api_key
    );

    let resp = reqwest::blocking::get(url);
    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<RolesJson>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };

    for p in result.values {
        if tab == 1 && p.sign_up_1 == "FALSE" {
            continue;
        }
        if tab == 2 && p.sign_up_2 == "FALSE" {
            continue;
        }
        if tab == 3 && p.sign_up_3 == "FALSE" {
            continue;
        }
        players.push(p);
    }
    players
}

pub(crate) fn read_predefined_groups() -> Vec<PredefinedGroup> {
    #[derive(Serialize, Deserialize, Debug)]
    struct PlayerJson {
        group_leader: String,
        group_type: String,
        player_1: String,
        player_2: String,
        player_3: String,
        player_4: String,
    }
    #[derive(Serialize, Deserialize, Debug)]
    #[allow(non_snake_case)]
    struct RolesJson {
        range: String,
        majorDimension: String,
        values: Vec<PlayerJson>,
    }

    let mut setup_filename = dirs::download_dir().expect("cannot detect path to download dir!");
    setup_filename.push("New-World-Extra - Setup.csv");

    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}",
        sheets_url, sheet_id, "/values/Setup!A2:F50", api_key
    );

    // let resp = reqwest::blocking::get(url);
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("User-Agent", "Rust Reqwest")
        .send();
    // ;        .expect("Could not access google sheets api!");

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<RolesJson>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };

    let mut setups: Vec<PredefinedGroup> = Vec::new();
    for group in result.values {
        let player_name = group.group_leader;
        if player_name.is_empty() {
            continue;
        }
        let player_group_type = group.group_type.trim().to_lowercase();
        let player_group_type = crate::util::string_to_group_type(player_group_type);
        let mut setup = PredefinedGroup {
            group_leader: player_name,
            group_type: player_group_type,
            group_members: vec![],
        };
        if !group.player_1.is_empty() {
            setup
                .group_members
                .push(group.player_1.trim().parse().unwrap());
        }
        if !group.player_2.is_empty() {
            setup
                .group_members
                .push(group.player_2.trim().parse().unwrap());
        }
        if !group.player_3.is_empty() {
            setup
                .group_members
                .push(group.player_3.trim().parse().unwrap());
        }
        if !group.player_4.is_empty() {
            setup
                .group_members
                .push(group.player_4.trim().parse().unwrap());
        }

        setups.push(setup);
    }
    setups
}

pub fn read_player_competence_rest() -> HashMap<String, usize> {
    #[derive(Serialize, Deserialize, Debug)]
    struct PlayerJson {
        name: String,
        skill: String,
    }
    #[derive(Serialize, Deserialize, Debug)]
    #[allow(non_snake_case)]
    struct RolesJson {
        range: String,
        majorDimension: String,
        values: Vec<PlayerJson>,
    }
    let mut player_competence_filename =
        dirs::download_dir().expect("cannot detect path to download dir!");
    player_competence_filename.push("New-World-Extra - Skills.csv");

    let mut player_name_to_competence: HashMap<String, usize> = HashMap::new();
    let sheets_url = "https://sheets.googleapis.com/v4/spreadsheets/";
    let sheet_id = "1GcKqn2XX4DfxtX6VP203aCLep725ZicgFGaq9QOBqx4";
    let api_key = format!("?key={}", crate::util::read_api_key());

    let url = format!(
        "{}{}{}{}",
        sheets_url, sheet_id, "/values/Skills!A2:B200", api_key
    );

    // let resp = reqwest::blocking::get(url);
    let resp = reqwest::blocking::Client::new()
        .get(url)
        .header("User-Agent", "Rust Reqwest")
        .send();
    // ;        .expect("Could not access google sheets api!");

    let response = match resp {
        Ok(response) => response,
        Err(err) => {
            panic!("Could not read data from google: {}", err)
        }
    };
    let result = match response.json::<RolesJson>() {
        Ok(result) => result,
        Err(err) => {
            panic!("Could not read json data from google: {}", err)
        }
    };

    for player in result.values {
        let player_name = player.name;
        if player_name.is_empty() {
            continue;
        }
        let player_skill = player.skill.parse::<usize>().expect(&*format!(
            "Skill of player {} is not a number!",
            &player_name
        ));

        if player_name_to_competence.contains_key(&*player_name) {
            println!(
                "player {} found more than once in competence list!",
                &player_name,
            );
            process::exit(1);
        }
        player_name_to_competence.insert(player_name, player_skill);
    }
    player_name_to_competence
}
