use std::fs::OpenOptions;
use std::io::Write;

use crate::data_types::{Raid, Role};
use crate::PlayersType;

pub(crate) fn write_result_to_csv_file(raid: &Raid, players: &PlayersType) {
    let file = OpenOptions::new()
        .read(true)
        .truncate(true)
        .write(true)
        .append(false)
        .create(true)
        .open("raid.csv")
        .expect("Could not create file!");

    if let Err(e) = writeln!(
        &file,
        "Type, Player 1, Placed, Player 2, Placed, Player 3, Placed, Player 4, Placed, Player 5, Placed"
    ) {
        eprintln!("Couldn't write to file: {}", e);
    }
    for g in &raid.groups {
        if let Err(e) = write!(&file, "{}{}", &*g.group_type.to_string(), ", ") {
            eprintln!("Couldn't write to file: {}", e);
        }
        for p in &g.players {
            if let Err(e) = write!(
                &file,
                "({}) {}, ,",
                players[p]
                    .calculated_role
                    .to_string()
                    .chars()
                    .next()
                    .unwrap(),
                p
            ) {
                eprintln!("Couldn't write to file: {}", e);
            }
        }
        if let Err(e) = writeln!(&file) {
            eprintln!("Couldn't write to file: {}", e);
        }
    }
}

#[allow(dead_code)]
pub(crate) fn write_unplaced_players_to_csv_file(_raid: &Raid, players: &PlayersType) {
    let file = OpenOptions::new()
        .read(true)
        .truncate(false)
        .write(true)
        .append(true)
        .create(true)
        .open("raid.csv")
        .expect("Could not create file!");

    // Todo can we do that more ideomatic?
    let mut tanks = vec![];
    for (name, _role) in players
        .iter()
        .filter(|&(_name, player)| player.signup_role == Role::Tank)
    {
        tanks.push(name.clone());
    }
    let mut melees = vec![];
    for (name, _role) in players
        .iter()
        .filter(|&(_name, player)| player.signup_role == Role::Melee)
    {
        melees.push(name.clone());
    }
    let mut ranges = vec![];
    for (name, _role) in players
        .iter()
        .filter(|&(_name, player)| player.signup_role == Role::Range)
    {
        ranges.push(name.clone());
    }
    let mut healers = vec![];
    for (name, _role) in players
        .iter()
        .filter(|&(_name, player)| player.signup_role == Role::Healer)
    {
        healers.push(name.clone());
    }

    if let Err(e) = writeln!(&file) {
        eprintln!("Couldn't write to file: {}", e);
    }
    if let Err(e) = writeln!(&file, "Tank, Melee, Range, Healer") {
        eprintln!("Couldn't write to file: {}", e);
    }

    while !tanks.is_empty() || !melees.is_empty() || !ranges.is_empty() || !healers.is_empty() {
        if let Err(e) = write!(
            &file,
            "{}, ",
            &tanks.pop().unwrap_or_else(|| "".parse().unwrap())
        ) {
            eprintln!("Couldn't write to file: {}", e);
        }
        if let Err(e) = write!(
            &file,
            "{}, ",
            &melees.pop().unwrap_or_else(|| "".parse().unwrap())
        ) {
            eprintln!("Couldn't write to file: {}", e);
        }
        if let Err(e) = write!(
            &file,
            "{}, ",
            &ranges.pop().unwrap_or_else(|| "".parse().unwrap())
        ) {
            eprintln!("Couldn't write to file: {}", e);
        }
        if let Err(e) = writeln!(
            &file,
            "{}, ",
            &healers.pop().unwrap_or_else(|| "".parse().unwrap())
        ) {
            eprintln!("Couldn't write to file: {}", e);
        }
    }
}
