Download the data:
	Open document New-World
	In tab Roles:
		File -> Download -> Comma-seperated values

	Open document New-World-Extra
	In tab Setup:
		File -> Download -> Comma-seperated values
	In tab Skills:
		File -> Download -> Comma-seperated values

After the 1st download the filename will be having a (1) at the end. Delete that which overwrites the 1st downloaded file.


Build the program (cd into the directory):
	git pull
	cargo run



Install the tools (only need this once):
git:	https://git-scm.com/downloads
rust:	https://rustup.rs/

Get the source code (the 1st time it will create a new directory):
	git clone https://gitlab.com/jbodemann/new-world-mmo-pvp-grouper.git
